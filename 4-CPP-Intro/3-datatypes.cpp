#include <iostream>

using namespace std;

int main() {

    cout << sizeof(char) << endl;
    cout << sizeof(int) << endl;
    cout << sizeof(float) << endl;
    cout << sizeof(double) << endl;

    cout << endl;

    int num = 300;
    char* c = (char*)(&num);

    for(int i = 0; i <  sizeof(int); i++) {
        cout << (int)c[i] << endl;
    }
}

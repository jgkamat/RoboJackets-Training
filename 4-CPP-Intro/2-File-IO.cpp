#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    const char* path;
    if(argc > 1) {
        path=argv[1];
    } else {
        path = "numbers.txt";
    }

    ifstream file(path);
    ofstream output("output.csv");

    while(file.is_open() && !file.eof()) {
        int num;
        file >> num;
        output << num << ',' << (num*num) << endl;
    }

    file.close();
    output.close();
}

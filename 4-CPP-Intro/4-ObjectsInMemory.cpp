#include <iostream>

using namespace std;

class Square {
    public:
        double width, height;
        int r, g, b, a;
};

int main() {

    Square s;
    s.width=100;
    s.height = 20.5;
    s.r = 255;
    s.g = 0;
    s.b = 0;
    s.a = 255;

    Square *addr = &s;
    cout << addr << endl;

    double* dub = (double*)addr;

    cout << *dub << endl;
    dub++;
    cout << *dub << endl;
    dub++;
    cout << *dub << endl;
    int *i = (int*)(dub);
    cout << *i << endl;
    i++;
    cout << *i << endl;

}

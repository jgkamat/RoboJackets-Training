
#include <iostream>

using namespace std;

int main() {
    cout << "How old are you?" << endl;

    int age;

    cin >> age;

    cout << "You are " << age << " years old!" << endl;
}
